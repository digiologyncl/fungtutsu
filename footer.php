<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fungtutsu
 */

?>

<?php if(is_single()) { ?>

  <section class="bg-faded text-xs-center" id="navigation">
    <div class="container">
      <span class="nav-link next"><?php next_post_link( '%link', '<svg class="dg dg-chevron-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dg-chevron-left"></use></svg>' ); ?></span>
      <span class="nav-link prev"><?php previous_post_link( '%link', '<svg class="dg dg-chevron-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dg-chevron-right"></use></svg>' ); ?></span>
    </div>
  </section>

<?php }; ?>

</div> <!-- /.site-body -->

<footer class="site-footer" id="footer">
  <div class="site-footer-inner container">
    <div class="row">
<?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
      <div class="col-xs-12 col-sm-6 col-lg-3">
<?php dynamic_sidebar( 'footer-1' ); ?>
      </div>
<?php } ?>
<?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
      <div class="col-xs-12 col-sm-6 col-lg-3">
<?php dynamic_sidebar( 'footer-2' ); ?>
      </div>
<?php } ?>
<?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
      <div class="col-xs-12 col-sm-6 col-lg-3">
<?php dynamic_sidebar( 'footer-3' ); ?>
      </div>
<?php } ?>
<?php if ( is_active_sidebar( 'footer-4' ) ) { ?>
      <div class="col-xs-12 col-sm-6 col-lg-3">
<?php dynamic_sidebar( 'footer-4' ); ?>
      </div>
<?php } ?>
    </div>
  </div>
</footer>

</div> <!-- /.site-content-inner -->
</div> <!-- /.site-content -->

<?php wp_footer(); ?>

</body>
</html>
