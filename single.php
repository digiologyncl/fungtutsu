<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fungtutsu
 */

get_header(); ?>

  <?php
    while ( have_posts() ) : the_post();
  ?>

  <section>
    <div class="container-xs">
      <?php get_template_part( 'template-parts/content', '' ); ?>
    </div>
  </section>


<?php
    endwhile; // End of the loop.
?>


<?php
get_footer();
