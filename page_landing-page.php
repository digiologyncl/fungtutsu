<?php
/**
 * Template Name: Landing Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fungtutsu
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<style type="text/css">

html,
body {
  min-height: 100%;
  background-color: #fff;
}

.landing-page {
  background: no-repeat center/cover fixed;
  display: table;
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%; /* For at least Firefox */
  min-height: 100vh;
  -webkit-box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
          box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
}
.landing-page-inner {
  min-height: 400px;
  display: table-cell;
  vertical-align: top;
  vertical-align: middle;
}
.landing-page-inner .container{
  padding-top: 4rem;
  padding-bottom: 4rem;
}

.site-copyright{
  display: none;
}

.landing-page-form-overlay{
  position: absolute;
  display: block;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0, 0);
  z-index: 9999999999999999;
  opacity: 0;
  -webkit-transition: background-color .3s ease-in-out .3s;
  transition: background-color .3s ease-in-out .3s;
  pointer-events: none;
}

.landing-page-form-overlay .landing-page-form{
  background-color: #eee;
  position: relative;
  width: 800px;
  max-width: 100%;
  padding: 2rem;
  text-align: center;
  margin: 0 auto;
  top: -100%;
  -webkit-transition: top .3s ease-in-out .6s;
  transition: top .3s ease-in-out .6s;
  border-radius: 3px;
}
.landing-page-form-overlay .landing-page-form .landing-page-form-inner{
  margin-top: 1rem;
}
.landing-page-form-overlay .landing-page-form .landing-page-form-close{
  position: absolute;
  top: .5rem;
  right: 1rem;
  cursor: pointer;
}
.landing-page-form-overlay .landing-page-form .landing-page-form-close:before{
  font-family: 'FontAwesome';
  content: '\f00d';
  font-size: 1rem;
  line-height: 1;
}
.form-is-active .landing-page-form-overlay,
.leaving-already .landing-page-form-overlay{
  opacity: 1;
  background-color: rgba(0,0,0, .75);
  pointer-events: auto;
}
.form-is-active .landing-page-form-overlay .landing-page-form,
.leaving-already .landing-page-form-overlay .landing-page-form{
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
}

</style>

</head>
<body <?php body_class(); ?> id="top">

<div style="display: none">
  <?php include_once( get_template_directory() . "/img/svg-icons.svg"); ?>
</div>

  <div class="landing-page" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
    <div class="landing-page-inner">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7 col-xl-6">
<?php
  while ( have_posts() ) : the_post();

    echo '<h1>' . get_the_title() . '</h1>';
    echo '<p class="lead"><strong>' . get_field('subtitle') . '</strong></p>';
    the_content();

  endwhile; // End of the loop.
?>
          </div>
        </div>
      </div>
    </div>
    <div class="landing-page-form-overlay">
      <div class="landing-page-form">
        <span class="landing-page-form-close"></span>
        <div class="landing-page-form-inner">
          <div class="card bg-white">
            <div class="card-content">
              <form id="offer_signup" method="POST" action="<?php echo get_the_permalink(); ?>thank-you/" style="text-align: left;">
                <input type="hidden" name="u" value="7f2f3ce5ded03855059b5533a">
                <input type="hidden" name="id" value="c9ceced188">
                <fieldset class="form-group">
                  <label for="MERGE1">Full Name <span>*</span></label>
                  <input type="text" name="MERGE1" id="MERGE1" class="form-control" placeholder="Your full name..." required>
                </fieldset>
                <fieldset class="form-group">
                  <label for="MERGE0" >Email Address <span>*</span></label>
                  <input type="email" name="MERGE0" id="MERGE0" class="form-control" placeholder="Your email address..." required>
                </fieldset>
                <fieldset class="form-group">
                  <label for="MERGE2" >Phone</label>
                  <input type="tel" name="MERGE2" id="MERGE2" class="form-control" placeholder="Your phone number...">
                </fieldset>
                <input type="text" name="the-page-id" class="form-control" value="<?php echo get_the_ID(); ?>" style="visibility: hidden; height: 0; border: 0; padding: 0;">
                <input type="text" name="hero-image" class="form-control" value="<?php echo get_the_post_thumbnail_url() ?>" style="visibility: hidden; height: 0; border: 0; padding: 0;">
                <input type="submit" name="offer-submit" class="btn btn-1 btn-block" value="Submit">
                <small><em>(* = Required fields)</em></small>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php show_sections(); ?>
  <?php show_banners(); ?>

<?php wp_footer(); ?>



<script>

(function($) {
  $(document).ready(function() {
    $(".landing-page-form-close").click(function(event) {
      $(".landing-page-form-overlay").fadeOut();
      $('body').removeClass('form-is-active');
    });
  });
  $(document).keyup(function(event) {
    if (event.keyCode === 27) $('.landing-page-form-close').click();
  });
  $(document).ready(function() {
    $('.landing-page-inner .btn').click(function(e) {
      e.preventDefault();
      $('body').addClass('leaving-already');
      $('body').addClass('form-is-active');
      $(".landing-page-form-overlay").fadeIn();
    });
  });
})( jQuery );

</script>

</body>
</html>
