<?php
/**
 * A Tom Ungerer Theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package fungtutsu
 */

if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
    require get_template_directory() . '/inc/back-compat.php';
    return;
}
require_once "inc/setup.php";
require_once "inc/enqueue.php";
require_once "inc/contact_form_7.php";
require_once "inc/images.php";
require_once "inc/updraftplus.php";


