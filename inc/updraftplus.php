<?php

add_filter('updraftplus_exclude_directory', 'my_updraftplus_exclude_directory', 10, 2);
function my_updraftplus_exclude_directory($filter, $dir) {
  return (basename($dir) == 'node_modules') ? true : $filter;
  return (basename($dir) == '.sass-cache') ? true : $filter;
}
add_filter('updraftplus_exclude_file', 'my_updraftplus_exclude_file', 10, 2);
function my_updraftplus_exclude_file($filter, $file) {
  return (basename($file) == 'sftp-config.json') ? true : $filter;
}