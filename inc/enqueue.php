<?php

/**
 * Enqueues scripts and styles.
 */

function fungtutsu_scripts() {

	// Theme description.
	wp_enqueue_style( 'fungtutsu-style', get_stylesheet_uri() );


	wp_enqueue_style( 'fungtutsu-styles', get_template_directory_uri() . '/css/style.css', array(), null );
	
	// Theme javascript.
	wp_enqueue_script( 'fungtutsu-javascript', get_template_directory_uri() . '/script.js', array( 'jquery' ), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'fungtutsu_scripts' );

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
