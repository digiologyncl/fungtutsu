module.exports = function(grunt) {

  // configure the tasks
  grunt.initConfig({

    pkg: grunt.file.readJSON( 'package.json' ),

    sass: {
      dist: {
        files: {
          'css/style.css' : 'css/scss/style.scss'
        }
      }
    },

    autoprefixer: {
      build: {
        options: {
          map: true
        },
        files: {
          'css/style.css': 'css/style.css',
        }
      }
    },

    imagemin: {
      icons: {
        files: [{
          expand: true,
          cwd: 'img/icons/',
          src: [ '**/*.svg' ],
          dest: 'img/icons/'
        }]
      }
    },

    svgstore:
    {
      options:
      {
        prefix : 'dg-',
        svg:
        {
          id : 'digicons',
          display : 'none',
          xmlns: 'http://www.w3.org/2000/svg'
        }
      },
      icons:
      {
        files:
        {
          'img/svg-icons.svg': [ 'img/icons/*.svg' , 'img/!icons/_*.svg'  ],
        }
      }
    },

    watch: {
      stylesheets: {
        files: 'css/scss/**/*.scss',
        tasks: [ 'stylesheets' ]
      },
      images: {
        files: [
          'img/icons/*.svg'
        ],
        tasks: [ 'images' ]
      }
    },

  });

  // load the tasks
  require( 'load-grunt-tasks' )(grunt);

  // define the tasks
  grunt.registerTask(
    'stylesheets',
    [ 'sass', 'autoprefixer' ]
  );

  grunt.registerTask(
    'images',
    [ 'imagemin:icons', 'svgstore:icons' ]
  );


  grunt.registerTask(
    'default',
    'Watches the project for changes, automatically builds them and runs a server.',
    [ 'images', 'stylesheets', 'watch' ]
  );
};