<form action="<?php echo home_url( '/' ); ?>" method="get" class="searchform">
  <label for="search" class="sr-only">Search in <?php echo bloginfo('name'); ?></label>
  <input class="form-control" type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search..." />
  <button type="submit" class="search-submit"><i class="fa fa-search"></i><span class="sr-only">Search</span></button>
</form>