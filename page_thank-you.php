<?php
/**
 * Template Name: Thank You
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fungtutsu
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<style type="text/css">

html,
body {
  min-height: 100%;
  background-color: transparent;
}

.thank-you {
  background: no-repeat center/cover fixed;
  display: table;
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%; /* For at least Firefox */
  min-height: 100vh;
  -webkit-box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
          box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
}
.thank-you-inner {
  min-height: 400px;
  display: table-cell;
  vertical-align: top;
  vertical-align: middle;
}
.thank-you-inner .container{
  padding-top: 4rem;
  padding-bottom: 4rem;
}

.site-copyright{
  display: none;
}

</style>

</head>
<body <?php body_class(); ?> id="top">

<div style="display: none">
  <?php include_once( get_template_directory() . '/img/svg-icons.svg'); ?>
</div>

  <div class="thank-you" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
    <div class="thank-you-inner">
      <div class="container-xs">
        <div class="card bg-white text-xs-center">
          <div class="card-content">
            <h1><?php the_title(); ?></h1>
            <p class="lead"><strong><?php the_field('subtitle'); ?></strong></p>
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php show_sections(); ?>
  <?php show_banners(); ?>

<?php wp_footer(); ?>

</body>
</html>
