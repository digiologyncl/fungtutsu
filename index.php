<?php
/**
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fungtutsu
 */

get_header();

// var
$id = get_field('main_content_id', get_option( 'page_for_posts' ));
$image = get_field('main_content_image', get_option( 'page_for_posts' ));
$container = get_field('main_content_container', get_option( 'page_for_posts' ));
$template = get_field('posts_page_template', get_option( 'page_for_posts' ));
if($template) {
  if($template == 'left') {
    $class = get_field('main_content_class', get_option( 'page_for_posts' )) . ' has-sidebar has-sidebar-left main-section';
  } elseif($template == 'right') {
    $class = get_field('main_content_class', get_option( 'page_for_posts' )) . ' has-sidebar has-sidebar-right main-section';
  } else {
    $class = get_field('main_content_class', get_option( 'page_for_posts' )) . ' main-section';
  }
} else {
  $class = get_field('main_content_class', get_option( 'page_for_posts' )) . ' main-section';
}

?>

      <section<?php if($class){ echo ' class="' . $class . '"'; } if($id){ echo ' id="' . $id . '"'; } ?>>
      <?php if($image){ echo '<div class="section-image" style="background-image:url(' . $image['url'] . ')"></div>'; } ?>
        <div class="container<?php if($container){ echo $container; } ?>">
        <?php if($template == 'left' || $template == 'right') : ?>
          <div class="row">
            <main class="col-xs-12 col-lg-8">
            <?php
              if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                  get_template_part( 'template-parts/content', '' );
                endwhile; // End of the loop.

                the_posts_pagination( array(
                  'prev_text' => '<span class="prev-page">' . __( '<i class="fa fa-angle-left"></i> Prev', 'funtutsu' ) . '</span>',
                  'next_text' => '<span class="next-page">' . __( 'Next <i class="fa fa-angle-right"></i>', 'funtutsu' ) . '</span>',
                  'before_page_number' => '<span class="sr-only">' . __( 'Page', 'funtutsu' ) . ' </span>',
                ) );
              else :
                get_template_part( 'template-parts/content', 'none' );
              endif;
            ?>
            </main>
            <aside class="col-xs-12 col-lg-4<?php if($template == 'left') { echo ' first-lg'; } ?>">
              <?php get_sidebar(); ?>
            </aside>
          </div>
        <?php else : ?>
            <?php
              if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                  get_template_part( 'template-parts/content', '' );
                endwhile; // End of the loop.

                the_posts_pagination( array(
                  'prev_text' => '<span class="prev-page">' . __( '<i class="fa fa-angle-left"></i> Prev', 'funtutsu' ) . '</span>',
                  'next_text' => '<span class="next-page">' . __( 'Next <i class="fa fa-angle-right"></i>', 'funtutsu' ) . '</span>',
                  'before_page_number' => '<span class="sr-only">' . __( 'Page', 'funtutsu' ) . ' </span>',
                ) );
              else :
                get_template_part( 'template-parts/content', 'none' );
              endif;
            ?>
        <?php endif; ?>
        </div>
      </section>

<?php
get_footer();