<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fungtutsu
 */

?>
  
<?php if(is_single()) : ?>

  <article class="single-blogpost">
    <?php if(get_the_post_thumbnail()) : ?>
      <div class="image-wrapper text-xs-center">
        <img src="<?php the_post_thumbnail_url('large'); ?>" alt="<?php the_title(); ?>" />
      </div>
    <?php endif; ?>
    <h1 class="title">
      <?php the_title(); ?>
    </h1>
    <div class="date">
      Written by <?php the_author(); ?> on <?php the_time(get_option('date_format')); ?>
    </div>
    <div class="content">
      <?php the_content(); ?>
    </div>
    <div class="tags">
      <?php the_tags( '<strong>Tags:</strong> ', ', ', '<br />' ); ?> 
    </div>
  </article>

<?php else : ?>

  <article class="blogpost<?php if(get_the_post_thumbnail()) { echo ' has-image'; } ?>">
    <div class="blogpost-inner">
      <div class="blogpost-image-wrapper">
        <div class="blogpost-image"<?php if(get_the_post_thumbnail()) : ?> style="background-image: url(<?php the_post_thumbnail_url('large'); ?>);"<?php endif; ?>></div>
      </div>
      <div class="blogpost-header">
        <h3 class="blogpost-title">
          <?php the_title(); ?>
        </h3>
        <div class="blogpost-date">
          <?php the_time(get_option('date_format')); ?>
        </div>
      </div>
      <div class="blogpost-body">
        <div class="blogpost-content">
        <?php
          the_excerpt();
        ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="blogpost-permalink">Read More</a>
      </div>
    </div>
  </article>
  
<?php endif; ?>