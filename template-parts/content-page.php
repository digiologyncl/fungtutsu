<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package fungtutsu
 */

// var
$id = get_field('main_content_id');
$image = get_field('main_content_image');
$container = get_field('main_content_container');
if(is_page_template('page_sidebar-left.php')) {
  $class = get_field('main_content_class') . ' has-sidebar has-sidebar-left';
} elseif(is_page_template('page_sidebar-right.php')) {
  $class = get_field('main_content_class') . ' has-sidebar has-sidebar-right';
} else {
  $class = get_field('main_content_class');
}
?>

<section class="main-section <?php if($class){ echo ' ' . $class; } ?>" <?php if($id){ echo ' id="' . $id . '"'; } ?>>
<?php if($image){ echo '<div class="section-image" style="background-image:url(' . $image['url'] . ')"></div>'; } ?>
  <div class="container<?php if($container){ echo $container; } ?>">
  <?php if(is_page_template('page_sidebar-left.php') || is_page_template('page_sidebar-right.php')) : ?>
    <div class="row">
      <main class="col-xs-12 col-lg-8">
        <?php the_content(); ?>
      </main>
      <aside class="col-xs-12 col-lg-4<?php if(is_page_template('page_sidebar-left.php')) { echo ' first-lg'; } ?>">
        <?php get_sidebar(); ?>
      </aside>
    </div>
  <?php else : ?>
    <?php the_content(); ?>
  <?php endif; ?>
  </div>
</section>