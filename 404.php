<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package fungtutsu
 */

get_header(); ?>

	<section>
		<div class="container">
			<h2>There's nothing to see here!</h2>
			<p>
				<?php esc_html_e( 'It looks like nothing was found at this location...', 'fungtutsu' ); ?>
			</p>
			<p>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-1">
					Back Home
				</a>
			</p>
		</div>
	</section>

<?php
get_footer();