<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fungtutsu
 */
$sidebar = get_field('page_sidebar');

if(is_page()) {
  if ($sidebar == 'two'){
    dynamic_sidebar( 'sidebar-3' );
  } elseif ($sidebar == 'three') {
    dynamic_sidebar( 'sidebar-4' );
  } else {
    dynamic_sidebar( 'sidebar-2' );
  }
} else {
  dynamic_sidebar( 'sidebar-1' );
}
