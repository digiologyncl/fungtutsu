(function($) {

  /* SLICK */
  $('.slick-home-slider').slick({
    fade: true,
    arrows: false,
    dots: false,
    autoplay: true,
    pauseOnHover: false,
    pauseOnFocus: false,
    autoplaySpeed: 3000,
    draggable: false
  });
  $('.slider-testimonials').slick({
    arrows: false,
    dots: false,
    autoplay: true,
    pauseOnHover: false,
    autoplaySpeed: 3000,
    slidesToScroll: 1,
    slidesToShow: 4,
    infinite: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.slider-affiliations').slick({
    arrows: false,
    dots: false,
    autoplay: true,
    pauseOnHover: false,
    autoplaySpeed: 3000,
    slidesToScroll: 1,
    slidesToShow: 5,
    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });




  $(window).bind("load resize",function(e){
    $('.card-service').css('height', 'auto');
    var contentHeight = 0;
    $('.card-service').each(function(){
      if ($(this).height() > contentHeight) { contentHeight = $(this).height(); }
    });
    $('.card-service').css('height', contentHeight+'px');

    $('.card-about').css('height', 'auto');
    var contentHeight = 0;
    $('.card-about').each(function(){
      if ($(this).height() > contentHeight) { contentHeight = $(this).height(); }
    });
    $('.card-about').css('height', contentHeight+'px');

    $('.testimonials .testimonial').css('height', 'auto');
    var contentHeight = 0;
    $('.testimonials .testimonial').each(function(){
      if ($(this).height() > contentHeight) { contentHeight = $(this).height(); }
    });
    $('.testimonials .testimonial').css('height', contentHeight+'px');
  });


})( jQuery );
